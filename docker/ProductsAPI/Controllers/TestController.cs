using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ProductsAPI.Controllers
{
    [Route("/api/test")]
    public class TestController : ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            return $"Echo from: {System.Environment.MachineName}";
        }

    }
}
